import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test_page/gen/assets.gen.dart';
import 'package:tomisha_test_page/l10n/l10n.dart';
import 'package:tomisha_test_page/test_page/view/widgets/header_view.dart';
import 'package:tomisha_test_page/test_page/view/widgets/step1.dart';
import 'package:tomisha_test_page/test_page/view/widgets/step2.dart';
import 'package:tomisha_test_page/test_page/view/widgets/step3.dart';
import 'package:tomisha_test_page/test_page/view/widgets/tomisha_app_bar.dart';
import 'package:tomisha_test_page/test_page/view/widgets/tomisha_bottom_bar.dart';
import 'package:tomisha_test_page/test_page/view/widgets/tomisha_tab.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';

class TestPage extends HookWidget {
  const TestPage({super.key});

  @override
  Widget build(BuildContext context) {
    final isRegisterButtonVisible = useMemoized(() => ValueNotifier(true));
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Positioned.fill(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        HeaderView(
                          isRegisterButtonVisible: isRegisterButtonVisible,
                        ),
                        const SizedBox(height: 70),
                        const PageContent(),
                      ],
                    ),
                  ),
                ),
                if (ScreenUtil().screenWidth < 600) const TomishaBottomBar(),
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: TomishaAppBar(
              isRegisterButtonVisible: isRegisterButtonVisible,
            ),
          ),
        ],
      ),
    );
  }
}

class PageContent extends HookWidget {
  const PageContent({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = useTabController(initialLength: 3);
    useListenable(controller);

    final String subtitle;
    final String step1text;
    final String step1svgPath;
    final String step2text;
    final String step2svgPath;
    final String step3text;
    final String step3svgPath;

    if (controller.index == 0) {
      subtitle = context.l10n.page1_subtitle;
      step1text = context.l10n.page1_step1_text;
      step1svgPath = Assets.mobile.undrawProfileDataReV81r.path;
      step2text = context.l10n.page1_step2_text;
      step2svgPath = Assets.mobile.undrawTask31wc.path;
      step3text = context.l10n.page1_step3_text;
      step3svgPath = Assets.mobile.undrawPersonalFile222m.path;
    } else if (controller.index == 1) {
      subtitle = context.l10n.page2_subtitle;
      step1text = context.l10n.page2_step1_text;
      step1svgPath = Assets.mobile.undrawProfileDataReV81r.path;
      step2text = context.l10n.page2_step2_text;
      step2svgPath = Assets.mobile.undrawAboutMeWa29.path;
      step3text = context.l10n.page2_step3_text;
      step3svgPath = Assets.mobile.undrawSwipeProfiles1I6mr.path;
    } else {
      subtitle = context.l10n.page3_subtitle;
      step1text = context.l10n.page3_step1_text;
      step1svgPath = Assets.mobile.undrawProfileDataReV81r.path;
      step2text = context.l10n.page3_step2_text;
      step2svgPath = Assets.mobile.undrawJobOffersKw5d.path;
      step3text = context.l10n.page3_step3_text;
      step3svgPath = Assets.mobile.undrawBusinessDealCpi9.path;
    }

    return Column(
      children: [
        TabBar(
          controller: controller,
          isScrollable: true,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          labelPadding: EdgeInsets.zero,
          indicator: const BoxDecoration(),
          onTap: (value) {
            controller.index = value;
          },
          tabs: [
            TomishaTab(
              text: context.l10n.page1_button,
              isSelected: controller.index == 0,
              isFirst: true,
            ),
            TomishaTab(
              text: context.l10n.page2_button,
              isSelected: controller.index == 1,
            ),
            TomishaTab(
              text: context.l10n.page3_button,
              isSelected: controller.index == 2,
              isLast: true,
            ),
          ],
        ),
        Text(
          subtitle,
          style: context.textStyles.subtitle.copyWith(
            color: context.colors.davyGrey,
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 20),
        Step1(
          svgPath: step1svgPath,
          text: step1text,
        ),
        const SizedBox(height: 32),
        Step2(
          svgPath: step2svgPath,
          text: step2text,
        ),
        const SizedBox(height: 32),
        Step3(
          svgPath: step3svgPath,
          text: step3text,
        ),
        const SizedBox(height: 32),
      ],
    );
  }
}
