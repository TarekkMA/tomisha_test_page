import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test_page/l10n/l10n.dart';
import 'package:tomisha_test_page/test_page/view/widgets/register_button.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';

class TomishaAppBar extends HookWidget {
  const TomishaAppBar({super.key, required this.isRegisterButtonVisible});

  final ValueNotifier<bool> isRegisterButtonVisible;

  @override
  Widget build(BuildContext context) {
    useListenable(isRegisterButtonVisible);
    return SafeArea(
      bottom: false,
      child: DecoratedBox(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadiusDirectional.only(
            bottomStart: Radius.circular(12),
            bottomEnd: Radius.circular(12),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(3, 6),
              blurRadius: 6,
              color: Color(0x29000000),
            )
          ],
        ),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 5,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xff319795),
                    Color(0xff3182CE),
                  ],
                  begin: AlignmentDirectional.centerStart,
                  end: AlignmentDirectional.centerEnd,
                ),
              ),
            ),
            Container(
              constraints: const BoxConstraints.tightForFinite(height: 67),
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(8, 12, 8, 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if (!isRegisterButtonVisible.value &&
                        ScreenUtil().screenWidth >= 600) ...[
                      Text(
                        context.l10n.appbar_text,
                        style: context.textStyles.normal.copyWith(
                          color: context.colors.davyGrey,
                          fontSize: 19,
                        ),
                      ),
                      10.horizontalSpace,
                      const RegisterButton(inAppbar: true),
                      5.horizontalSpace,
                    ],
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        context.l10n.login_button,
                        style: context.textStyles.button.copyWith(
                          color: context.colors.dustyTeal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
