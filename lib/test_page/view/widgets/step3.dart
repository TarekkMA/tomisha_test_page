import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';
import 'package:tomisha_test_page/utils/exts.dart';

class Step3 extends StatelessWidget {

  const Step3({
    super.key,
    required this.svgPath,
    required this.text,
  });
  final String svgPath;
  final String text;

  @override
  Widget build(BuildContext context) {
    final image = SvgPicture.asset(
      svgPath,
      width: 0.25.sw.between(280, 300),
    );
    final stepText = Text(
      text,
      style:
      context.textStyles.normal.copyWith(color: context.colors.steelGray),
      textAlign: TextAlign.center,
    );
    final number = CustomPaint(
      painter: Step3Painter(),
      child: Text(
        '3.',
        style: context.textStyles.bigNumbers
            .copyWith(color: context.colors.steelGray),
      ),
    );
    return ScreenUtil().screenWidth < 600
        ? Column(
      children: [
        Row(
          textBaseline: TextBaseline.ideographic,
          children: [
            SizedBox(width: 0.18.sw),
            number,
            Expanded(
              child: stepText,
            ),
            SizedBox(width: 0.05.sw),
          ],
        ),
        Row(
          children: [
            const Spacer(flex: 3),
            image,
            const Spacer(),
          ],
        ),
      ],
    )
        : Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        number,
        5.horizontalSpace,
        Flexible(child: stepText),
        20.horizontalSpace,
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 20),
          child: image,
        ),
      ],
    );
  }
}

class Step3Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    const height = 304.0;
    const width = 304.0;
    canvas.drawOval(
      const Rect.fromLTWH(250 - 304 - 100, 0, width, height),
      Paint()..color = TomishaColors().ghostWhite,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
