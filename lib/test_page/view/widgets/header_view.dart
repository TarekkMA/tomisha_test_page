
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test_page/gen/assets.gen.dart';
import 'package:tomisha_test_page/l10n/l10n.dart';
import 'package:tomisha_test_page/test_page/view/widgets/register_button.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';
import 'package:visibility_detector/visibility_detector.dart';

class HeaderView extends HookWidget {

  const HeaderView({super.key, required this.isRegisterButtonVisible});
  final ValueNotifier<bool> isRegisterButtonVisible;

  @override
  Widget build(BuildContext context) {
    final text = Text(
      context.l10n.page_title,
      style: context.textStyles.title.copyWith(
        color: context.colors.darkSlate,
      ),
      textAlign: TextAlign.center,
    );

    final image = ScreenUtil().screenWidth < 600
        ? Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Transform.scale(
        scale: 1.05,
        child: Assets.mobile.undrawAgreementAajr.svg(
          width: 1.sw,
        ),
      ),
    )
        : Container(
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: context.colors.white,
        shape: BoxShape.circle,
      ),
      child: Assets.undrawAgreementAajr.svg(
        width: min(0.4.sw, 500),
      ),
    );

    return ClipPath(
      clipper: HeaderClipper(),
      child: Container(
        padding: const EdgeInsets.only(bottom: 50, top: 90),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xffEBF4FF),
              Color(0xffE6FFFA),
            ],
            begin: AlignmentDirectional.topStart,
            end: AlignmentDirectional.bottomEnd,
          ),
        ),
        child: ScreenUtil().screenWidth < 600
            ? Column(
          children: [
            text,
            image,
          ],
        )
            : Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                text,
                48.verticalSpace,
                VisibilityDetector(
                  key: const Key('register_button'),
                  onVisibilityChanged: (visibilityInfo) {
                    if (visibilityInfo.visibleFraction < 0.2) {
                      isRegisterButtonVisible.value = false;
                    } else {
                      isRegisterButtonVisible.value = true;
                    }
                  },
                  child: const RegisterButton(),
                ),
              ],
            ),
            image,
          ],
        ),
      ),
    );
  }
}


class HeaderClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    const controlPointOffset = 50;
    final cpoint1x = size.width / 4;
    final cpoint2x = size.width / 4 * 3;

    return Path()
      ..moveTo(0, 0)
      ..lineTo(0, size.height)
      ..cubicTo(
        cpoint1x,
        size.height - controlPointOffset,
        cpoint2x,
        size.height + controlPointOffset,
        size.width,
        size.height - 50,
      )
      ..lineTo(size.width, 0);
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
