import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';
import 'package:tomisha_test_page/utils/exts.dart';

class Step2 extends StatelessWidget {

  const Step2({
    super.key,
    required this.svgPath,
    required this.text,
  });
  final String svgPath;
  final String text;

  @override
  Widget build(BuildContext context) {
    final image = SvgPicture.asset(
      svgPath,
      width: 0.25.sw.between(180, 350),
    );
    final number = Text(
      '2.',
      style: context.textStyles.bigNumbers
          .copyWith(color: context.colors.steelGray),
    );
    final stepText = Text(
      text,
      style:
      context.textStyles.normal.copyWith(color: context.colors.steelGray),
      textAlign: TextAlign.center,
    );
    return CustomPaint(
      painter: Step2Painter(),
      child: ScreenUtil().screenWidth < 600
          ? Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              SizedBox(width: 0.13.sw),
              number,
              Expanded(
                child: stepText,
              ),
              SizedBox(width: 0.05.sw),
            ],
          ),
          Row(
            children: [
              const Spacer(flex: 3),
              image,
              const Spacer(flex: 2),
            ],
          ),
        ],
      )
          : Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.baseline,
        textBaseline: TextBaseline.alphabetic,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20),
            child: image,
          ),
          20.horizontalSpace,
          number,
          5.horizontalSpace,
          Flexible(child: stepText),
        ],
      ),
    );
  }
}

class Step2Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..shader = ui.Gradient.linear(
        Offset.zero,
        Offset(size.width, size.height),
        [
          const ui.Color(0xFFE6FFFA),
          const ui.Color(0xFFEBF4FF),
        ],
      );

    const controlPointOffset = 25.0;
    final cpoint1x = size.width / 10 * 2;
    final cpoint2x = size.width / 10 * 4;
    final cpoint3x = size.width / 10 * 6;
    final cpoint4x = size.width / 10 * 8;

    final path = Path()
      ..moveTo(0, 20)
      ..cubicTo(
        cpoint1x,
        -controlPointOffset / 2,
        cpoint2x,
        controlPointOffset / 2,
        size.width / 2,
        10,
      )
      ..cubicTo(
        cpoint3x,
        controlPointOffset,
        cpoint4x,
        -controlPointOffset / 3,
        size.width,
        100,
      )
      ..lineTo(size.width, 50)
      ..lineTo(size.width, size.height)
      ..cubicTo(
        size.width * 0.9,
        size.height - 100,
        size.width * 0.1,
        size.height + 50,
        0,
        size.height,
      );
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
