import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';
import 'package:tomisha_test_page/utils/exts.dart';

class Step1 extends StatelessWidget {

  const Step1({
    super.key,
    required this.svgPath,
    required this.text,
  });
  final String svgPath;
  final String text;

  @override
  Widget build(BuildContext context) {
    final image = SvgPicture.asset(
      svgPath,
      width: 0.25.sw.between(200, 400),
    );
    final number = CustomPaint(
      painter: Step1Painter(),
      child: Text(
        '1.',
        style: context.textStyles.bigNumbers
            .copyWith(color: context.colors.steelGray),
      ),
    );
    final stepText = Text(
      text,
      style:
      context.textStyles.normal.copyWith(color: context.colors.steelGray),
      textAlign: TextAlign.center,
    );
    return ScreenUtil().screenWidth < 600
        ? Stack(
      children: [
        Row(
          children: [
            const Spacer(flex: 2),
            image,
            const Spacer(),
          ],
        ),
        Column(
          children: [
            const SizedBox(height: 50),
            Row(
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: [
                SizedBox(width: 0.08.sw),
                number,
                Expanded(
                  child: stepText,
                ),
                SizedBox(width: 0.05.sw),
              ],
            ),
          ],
        ),
      ],
    )
        : Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        number,
        5.horizontalSpace,
        Flexible(child: stepText),
        20.horizontalSpace,
        Padding(
          padding: const EdgeInsets.only(top: 20, right: 20),
          child: image,
        ),
      ],
    );
  }
}

class Step1Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    const height = 208.0;
    const width = 208.0;
    canvas.drawOval(
      Rect.fromLTWH(174 - 208 - 50, size.height - height + 25, width, height),
      Paint()..color = TomishaColors().ghostWhite.withOpacity(0.5),
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
