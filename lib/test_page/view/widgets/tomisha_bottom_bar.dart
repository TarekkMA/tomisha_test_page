import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tomisha_test_page/test_page/view/widgets/register_button.dart';

class TomishaBottomBar extends StatelessWidget {
  const TomishaBottomBar({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 1.sw,
      child: DecoratedBox(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadiusDirectional.only(
            topStart: Radius.circular(12),
            topEnd: Radius.circular(12),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -1),
              blurRadius: 3,
              color: Color(0x29000000),
            )
          ],
        ),
        child: Padding(
          padding: EdgeInsetsDirectional.fromSTEB(
              20, 20, 20, 20 + MediaQuery.of(context).padding.bottom,),
          child: const RegisterButton(),
        ),
      ),
    );
  }
}
