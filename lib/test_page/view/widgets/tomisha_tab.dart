import 'package:flutter/material.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';

class TomishaTab extends StatelessWidget {

  const TomishaTab({
    super.key,
    required this.text,
    required this.isSelected,
    this.isFirst = false,
    this.isLast = false,
  });
  final String text;
  final bool isSelected;
  final bool isFirst;
  final bool isLast;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        color: isSelected ? context.colors.lightTeal : context.colors.white,
        border: Border.all(color: context.colors.lightTeal),
        borderRadius: BorderRadiusDirectional.only(
          topStart: isFirst ? const Radius.circular(12) : Radius.zero,
          bottomStart: isFirst ? const Radius.circular(12) : Radius.zero,
          topEnd: isLast ? const Radius.circular(12) : Radius.zero,
          bottomEnd: isLast ? const Radius.circular(12) : Radius.zero,
        ),
      ),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 38),
      child: Text(
        text,
        style: context.textStyles.button.copyWith(
          color: isSelected
              ? context.colors.frostedMint
              : context.colors.dustyTeal,
        ),
      ),
    );
  }
}
