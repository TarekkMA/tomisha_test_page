import 'package:flutter/material.dart';
import 'package:tomisha_test_page/l10n/l10n.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';

class RegisterButton extends StatelessWidget {

  const RegisterButton({
    super.key,
    this.inAppbar = false,
  });
  final bool inAppbar;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        alignment: Alignment.center,
        padding:
        EdgeInsets.symmetric(vertical: 12, horizontal: inAppbar ? 38 : 48),
        decoration: BoxDecoration(
          gradient: inAppbar
              ? null
              : const LinearGradient(
            colors: [
              Color(0xff319795),
              Color(0xff3182CE),
            ],
            begin: AlignmentDirectional.centerStart,
            end: AlignmentDirectional.centerEnd,
          ),
          border: !inAppbar
              ? null
              : Border.all(
            color: context.colors.paleAqua,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Text(
          context.l10n.register_button,
          style: context.textStyles.button.copyWith(
            color: inAppbar
                ? context.colors.dustyTeal
                : context.colors.frostedMint,
          ),
        ),
      ),
    );
  }
}
