import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_fonts/google_fonts.dart';

part 'theme_cubit.freezed.dart';

extension ThemeX on BuildContext {
  TomishaColors get colors => read<ThemeCubit>().state.colors;

  TomishaTextStyles get textStyles => read<ThemeCubit>().state.textStyles;
}

@freezed
class TomishaTheme with _$TomishaTheme {
  const factory TomishaTheme({
    required TomishaTextStyles textStyles,
    required TomishaColors colors,
  }) = _TomishaTheme;

  factory TomishaTheme.mobile() => TomishaTheme(
        textStyles: TomishaTextStyles.mobile(),
        colors: TomishaColors(),
      );

  factory TomishaTheme.desktop() => TomishaTheme(
        textStyles: TomishaTextStyles.desktop(),
        colors: TomishaColors(),
      );
}

@freezed
class TomishaTextStyles with _$TomishaTextStyles {
  const factory TomishaTextStyles({
    required TextStyle button,
    required TextStyle title,
    required TextStyle subtitle,
    required TextStyle normal,
    required TextStyle bigNumbers,
  }) = _TomishaTextStyles;

  factory TomishaTextStyles.mobile() => TomishaTextStyles(
        button: GoogleFonts.lato(
          fontWeight: FontWeight.w600,
          fontSize: 14,
          height: 17 / 14,
          letterSpacing: 0.84,
        ),
        title: GoogleFonts.lato(
          fontWeight: FontWeight.w500,
          fontSize: 42,
          height: 50 / 42,
          letterSpacing: 1.26,
        ),
        subtitle: GoogleFonts.lato(
          fontWeight: FontWeight.w500,
          fontSize: 21,
          height: 25 / 21,
          letterSpacing: 0,
        ),
        normal: GoogleFonts.lato(
          fontWeight: FontWeight.normal,
          fontSize: 16,
          height: 19 / 16,
          letterSpacing: 0.47,
        ),
        bigNumbers: GoogleFonts.lato(
          fontWeight: FontWeight.normal,
          fontSize: 130,
          height: 156 / 130,
          letterSpacing: 0,
        ),
      );

  factory TomishaTextStyles.desktop() => TomishaTextStyles(
        button: GoogleFonts.lato(
          fontWeight: FontWeight.w600,
          fontSize: 14,
          height: 17 / 14,
          letterSpacing: 0.84,
        ),
        title: GoogleFonts.lato(
          fontWeight: FontWeight.bold,
          fontSize: 65,
          height: 78 / 65,
          letterSpacing: 1.95,
        ),
        subtitle: GoogleFonts.lato(
          fontWeight: FontWeight.w500,
          fontSize: 40,
          height: 48 / 40,
          letterSpacing: 0,
        ),
        normal: GoogleFonts.lato(
          fontWeight: FontWeight.normal,
          fontSize: 30,
          height: 36 / 30,
          letterSpacing: 0.9,
        ),
        bigNumbers: GoogleFonts.lato(
          fontWeight: FontWeight.normal,
          fontSize: 130,
          height: 156 / 130,
          letterSpacing: 0,
        ),
      );
}

class TomishaColors {
  final white = const Color(0xffffffff);
  final davyGrey = const Color(0xff4A5568);
  final ghostWhite = const Color(0xffF7FAFC);
  final steelGray = const Color(0xff718096);
  final frostedMint = const Color(0xFFE6FFFA);
  final lilyWhite = const Color(0xFFEBF4FF);
  final darkSlate = const Color(0xFF2D3748);
  final paleAqua = const Color(0xFFCBD5E0);
  final lightTeal = const Color(0xFF81E6D9);
  final dustyTeal = const Color(0xFF319795);
  final tuftsBlue = const Color(0xFF3182CE);
  final smokeyGrey = const Color(0xFF707070);
}

class ThemeCubit extends Cubit<TomishaTheme> {
  ThemeCubit(super.initialState);

  void changeTheme(TomishaTheme newTheme) {
    if (state != newTheme) emit(newTheme);
  }
}
