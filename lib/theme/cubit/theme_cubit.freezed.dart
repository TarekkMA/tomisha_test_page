// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'theme_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TomishaTheme {
  TomishaTextStyles get textStyles => throw _privateConstructorUsedError;
  TomishaColors get colors => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TomishaThemeCopyWith<TomishaTheme> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TomishaThemeCopyWith<$Res> {
  factory $TomishaThemeCopyWith(
          TomishaTheme value, $Res Function(TomishaTheme) then) =
      _$TomishaThemeCopyWithImpl<$Res, TomishaTheme>;
  @useResult
  $Res call({TomishaTextStyles textStyles, TomishaColors colors});

  $TomishaTextStylesCopyWith<$Res> get textStyles;
}

/// @nodoc
class _$TomishaThemeCopyWithImpl<$Res, $Val extends TomishaTheme>
    implements $TomishaThemeCopyWith<$Res> {
  _$TomishaThemeCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? textStyles = null,
    Object? colors = null,
  }) {
    return _then(_value.copyWith(
      textStyles: null == textStyles
          ? _value.textStyles
          : textStyles // ignore: cast_nullable_to_non_nullable
              as TomishaTextStyles,
      colors: null == colors
          ? _value.colors
          : colors // ignore: cast_nullable_to_non_nullable
              as TomishaColors,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $TomishaTextStylesCopyWith<$Res> get textStyles {
    return $TomishaTextStylesCopyWith<$Res>(_value.textStyles, (value) {
      return _then(_value.copyWith(textStyles: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_TomishaThemeCopyWith<$Res>
    implements $TomishaThemeCopyWith<$Res> {
  factory _$$_TomishaThemeCopyWith(
          _$_TomishaTheme value, $Res Function(_$_TomishaTheme) then) =
      __$$_TomishaThemeCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({TomishaTextStyles textStyles, TomishaColors colors});

  @override
  $TomishaTextStylesCopyWith<$Res> get textStyles;
}

/// @nodoc
class __$$_TomishaThemeCopyWithImpl<$Res>
    extends _$TomishaThemeCopyWithImpl<$Res, _$_TomishaTheme>
    implements _$$_TomishaThemeCopyWith<$Res> {
  __$$_TomishaThemeCopyWithImpl(
      _$_TomishaTheme _value, $Res Function(_$_TomishaTheme) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? textStyles = null,
    Object? colors = null,
  }) {
    return _then(_$_TomishaTheme(
      textStyles: null == textStyles
          ? _value.textStyles
          : textStyles // ignore: cast_nullable_to_non_nullable
              as TomishaTextStyles,
      colors: null == colors
          ? _value.colors
          : colors // ignore: cast_nullable_to_non_nullable
              as TomishaColors,
    ));
  }
}

/// @nodoc

class _$_TomishaTheme implements _TomishaTheme {
  const _$_TomishaTheme({required this.textStyles, required this.colors});

  @override
  final TomishaTextStyles textStyles;
  @override
  final TomishaColors colors;

  @override
  String toString() {
    return 'TomishaTheme(textStyles: $textStyles, colors: $colors)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TomishaTheme &&
            (identical(other.textStyles, textStyles) ||
                other.textStyles == textStyles) &&
            (identical(other.colors, colors) || other.colors == colors));
  }

  @override
  int get hashCode => Object.hash(runtimeType, textStyles, colors);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TomishaThemeCopyWith<_$_TomishaTheme> get copyWith =>
      __$$_TomishaThemeCopyWithImpl<_$_TomishaTheme>(this, _$identity);
}

abstract class _TomishaTheme implements TomishaTheme {
  const factory _TomishaTheme(
      {required final TomishaTextStyles textStyles,
      required final TomishaColors colors}) = _$_TomishaTheme;

  @override
  TomishaTextStyles get textStyles;
  @override
  TomishaColors get colors;
  @override
  @JsonKey(ignore: true)
  _$$_TomishaThemeCopyWith<_$_TomishaTheme> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TomishaTextStyles {
  TextStyle get button => throw _privateConstructorUsedError;
  TextStyle get title => throw _privateConstructorUsedError;
  TextStyle get subtitle => throw _privateConstructorUsedError;
  TextStyle get normal => throw _privateConstructorUsedError;
  TextStyle get bigNumbers => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TomishaTextStylesCopyWith<TomishaTextStyles> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TomishaTextStylesCopyWith<$Res> {
  factory $TomishaTextStylesCopyWith(
          TomishaTextStyles value, $Res Function(TomishaTextStyles) then) =
      _$TomishaTextStylesCopyWithImpl<$Res, TomishaTextStyles>;
  @useResult
  $Res call(
      {TextStyle button,
      TextStyle title,
      TextStyle subtitle,
      TextStyle normal,
      TextStyle bigNumbers});
}

/// @nodoc
class _$TomishaTextStylesCopyWithImpl<$Res, $Val extends TomishaTextStyles>
    implements $TomishaTextStylesCopyWith<$Res> {
  _$TomishaTextStylesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? button = null,
    Object? title = null,
    Object? subtitle = null,
    Object? normal = null,
    Object? bigNumbers = null,
  }) {
    return _then(_value.copyWith(
      button: null == button
          ? _value.button
          : button // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      subtitle: null == subtitle
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      normal: null == normal
          ? _value.normal
          : normal // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      bigNumbers: null == bigNumbers
          ? _value.bigNumbers
          : bigNumbers // ignore: cast_nullable_to_non_nullable
              as TextStyle,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TomishaTextStylesCopyWith<$Res>
    implements $TomishaTextStylesCopyWith<$Res> {
  factory _$$_TomishaTextStylesCopyWith(_$_TomishaTextStyles value,
          $Res Function(_$_TomishaTextStyles) then) =
      __$$_TomishaTextStylesCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {TextStyle button,
      TextStyle title,
      TextStyle subtitle,
      TextStyle normal,
      TextStyle bigNumbers});
}

/// @nodoc
class __$$_TomishaTextStylesCopyWithImpl<$Res>
    extends _$TomishaTextStylesCopyWithImpl<$Res, _$_TomishaTextStyles>
    implements _$$_TomishaTextStylesCopyWith<$Res> {
  __$$_TomishaTextStylesCopyWithImpl(
      _$_TomishaTextStyles _value, $Res Function(_$_TomishaTextStyles) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? button = null,
    Object? title = null,
    Object? subtitle = null,
    Object? normal = null,
    Object? bigNumbers = null,
  }) {
    return _then(_$_TomishaTextStyles(
      button: null == button
          ? _value.button
          : button // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      subtitle: null == subtitle
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      normal: null == normal
          ? _value.normal
          : normal // ignore: cast_nullable_to_non_nullable
              as TextStyle,
      bigNumbers: null == bigNumbers
          ? _value.bigNumbers
          : bigNumbers // ignore: cast_nullable_to_non_nullable
              as TextStyle,
    ));
  }
}

/// @nodoc

class _$_TomishaTextStyles implements _TomishaTextStyles {
  const _$_TomishaTextStyles(
      {required this.button,
      required this.title,
      required this.subtitle,
      required this.normal,
      required this.bigNumbers});

  @override
  final TextStyle button;
  @override
  final TextStyle title;
  @override
  final TextStyle subtitle;
  @override
  final TextStyle normal;
  @override
  final TextStyle bigNumbers;

  @override
  String toString() {
    return 'TomishaTextStyles(button: $button, title: $title, subtitle: $subtitle, normal: $normal, bigNumbers: $bigNumbers)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TomishaTextStyles &&
            (identical(other.button, button) || other.button == button) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.subtitle, subtitle) ||
                other.subtitle == subtitle) &&
            (identical(other.normal, normal) || other.normal == normal) &&
            (identical(other.bigNumbers, bigNumbers) ||
                other.bigNumbers == bigNumbers));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, button, title, subtitle, normal, bigNumbers);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TomishaTextStylesCopyWith<_$_TomishaTextStyles> get copyWith =>
      __$$_TomishaTextStylesCopyWithImpl<_$_TomishaTextStyles>(
          this, _$identity);
}

abstract class _TomishaTextStyles implements TomishaTextStyles {
  const factory _TomishaTextStyles(
      {required final TextStyle button,
      required final TextStyle title,
      required final TextStyle subtitle,
      required final TextStyle normal,
      required final TextStyle bigNumbers}) = _$_TomishaTextStyles;

  @override
  TextStyle get button;
  @override
  TextStyle get title;
  @override
  TextStyle get subtitle;
  @override
  TextStyle get normal;
  @override
  TextStyle get bigNumbers;
  @override
  @JsonKey(ignore: true)
  _$$_TomishaTextStylesCopyWith<_$_TomishaTextStyles> get copyWith =>
      throw _privateConstructorUsedError;
}
