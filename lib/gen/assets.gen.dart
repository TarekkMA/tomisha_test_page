/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

class $AssetsMobileGen {
  const $AssetsMobileGen();

  /// File path: assets/mobile/undraw_Profile_data_re_v81r.svg
  SvgGenImage get undrawProfileDataReV81r =>
      const SvgGenImage('assets/mobile/undraw_Profile_data_re_v81r.svg');

  /// File path: assets/mobile/undraw_about_me_wa29.svg
  SvgGenImage get undrawAboutMeWa29 =>
      const SvgGenImage('assets/mobile/undraw_about_me_wa29.svg');

  /// File path: assets/mobile/undraw_agreement_aajr.svg
  SvgGenImage get undrawAgreementAajr =>
      const SvgGenImage('assets/mobile/undraw_agreement_aajr.svg');

  /// File path: assets/mobile/undraw_business_deal_cpi9.svg
  SvgGenImage get undrawBusinessDealCpi9 =>
      const SvgGenImage('assets/mobile/undraw_business_deal_cpi9.svg');

  /// File path: assets/mobile/undraw_job_offers_kw5d.svg
  SvgGenImage get undrawJobOffersKw5d =>
      const SvgGenImage('assets/mobile/undraw_job_offers_kw5d.svg');

  /// File path: assets/mobile/undraw_personal_file_222m.svg
  SvgGenImage get undrawPersonalFile222m =>
      const SvgGenImage('assets/mobile/undraw_personal_file_222m.svg');

  /// File path: assets/mobile/undraw_swipe_profiles1_i6mr.svg
  SvgGenImage get undrawSwipeProfiles1I6mr =>
      const SvgGenImage('assets/mobile/undraw_swipe_profiles1_i6mr.svg');

  /// File path: assets/mobile/undraw_task_31wc.svg
  SvgGenImage get undrawTask31wc =>
      const SvgGenImage('assets/mobile/undraw_task_31wc.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        undrawProfileDataReV81r,
        undrawAboutMeWa29,
        undrawAgreementAajr,
        undrawBusinessDealCpi9,
        undrawJobOffersKw5d,
        undrawPersonalFile222m,
        undrawSwipeProfiles1I6mr,
        undrawTask31wc
      ];
}

class Assets {
  Assets._();

  static const $AssetsMobileGen mobile = $AssetsMobileGen();
  static const SvgGenImage undrawAgreementAajr =
      SvgGenImage('assets/undraw_agreement_aajr.svg');

  /// List of all assets
  List<SvgGenImage> get values => [undrawAgreementAajr];
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
