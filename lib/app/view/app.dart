import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:tomisha_test_page/l10n/l10n.dart';
import 'package:tomisha_test_page/test_page/test_page.dart';
import 'package:tomisha_test_page/theme/cubit/theme_cubit.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeCubit>(
      create: (context) => ThemeCubit(TomishaTheme.mobile()),
      child: MultiProvider(
        providers: [
          Provider(create: (context) => context.read<ThemeCubit>().state),
          Provider(
            create: (context) => context.read<ThemeCubit>().state.textStyles,
          ),
          Provider(
            create: (context) => context.read<ThemeCubit>().state.colors,
          ),
        ],
        child: ScreenUtilInit(
          builder: (BuildContext context, Widget? child) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: _buildTheme(Brightness.light),
              localizationsDelegates: AppLocalizations.localizationsDelegates,
              supportedLocales: AppLocalizations.supportedLocales,
              builder: (context, child) => LayoutBuilder(
                builder: (context, constraints) {
                  context.read<ThemeCubit>().changeTheme(
                        constraints.maxWidth > 600
                            ? TomishaTheme.desktop()
                            : TomishaTheme.mobile(),
                      );
                  return child!;
                },
              ),
              home: child,
            );
          },
          child: const TestPage(),
        ),
      ),
    );
  }
}

ThemeData _buildTheme(Brightness brightness) {
  final baseTheme = ThemeData(brightness: brightness);

  return baseTheme.copyWith(
    textTheme: GoogleFonts.latoTextTheme(baseTheme.textTheme),
  );
}
