import 'package:tomisha_test_page/app/app.dart';
import 'package:tomisha_test_page/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
