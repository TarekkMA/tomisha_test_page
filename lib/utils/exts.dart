extension DoubleX on double {
  double between(double min, double max) {
    if (this > max) return max;
    if (this < min) return min;
    return this;
  }
}
